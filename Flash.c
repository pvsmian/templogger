#include <string.h>

#include "Flash.h"
#include "app_error.h"
#include "nrf_soc.h"

enum FlashOperation
{
	FlashOperationNone
	, FlashOperationErase
	, FlashOperationWrite
};

static int volatile g_FlashOperation = 0;
static FlashCallback * g_EraseCallback = 0;
static FlashCallback * g_WriteCallback = 0;
	
uint32_t FlashErasePage(uint32_t _page_address)
{
	if(g_FlashOperation == FlashOperationNone)
	{
		uint32_t err_code = sd_flash_page_erase(_page_address / 1024);
		if(err_code != NRF_SUCCESS)
			return err_code;
		g_FlashOperation = FlashOperationErase;
		return err_code;
	}
	return NRF_ERROR_INVALID_STATE;
}

uint32_t FlashWrite(uint32_t _address, const uint8_t * _data, uint32_t _len)
{
	if(g_FlashOperation == FlashOperationNone)
	{
		if(_len % 4 != 0)
			return NRF_ERROR_INVALID_LENGTH;
		
		uint32_t err_code = sd_flash_write((uint32_t * const)_address, (uint32_t const * const)_data, _len / 4);
		if(err_code != NRF_SUCCESS)
			return err_code;
		g_FlashOperation = FlashOperationWrite;
		return err_code;
	}	
	return NRF_ERROR_INVALID_STATE;
}

void FlashHandleSysEvent(uint8_t _evt)
{
	switch(_evt)
    {
        case NRF_EVT_FLASH_OPERATION_ERROR:
        case NRF_EVT_FLASH_OPERATION_SUCCESS:
			if(g_FlashOperation == FlashOperationErase)
			{
				g_FlashOperation = FlashOperationNone;
				if(g_EraseCallback != 0)
					g_EraseCallback(_evt == NRF_EVT_FLASH_OPERATION_SUCCESS);
			}
			else if(g_FlashOperation == FlashOperationWrite)
			{
				g_FlashOperation = FlashOperationNone;
				if(g_WriteCallback != 0)
					g_WriteCallback(_evt == NRF_EVT_FLASH_OPERATION_SUCCESS);
			}
			break;
        default:
            break;
    }
}

void FlashRead(uint32_t _address, uint8_t * _data, uint32_t _len)
{
	const uint8_t * d = (const uint8_t * )_address;
	for(int i = 0; i < _len; ++i)
		*_data++ = *d++;
	
	//memcpy(_data, d, _len);
}

void FlashSetEraseCallback(void (*_callback) (bool))
{
	g_EraseCallback = _callback;
}

void FlashSetWriteCallback(void (*_callback) (bool))
{
	g_WriteCallback = _callback;
}

