#include "ble_hci.h"  
#include "ble_advdata.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "AdvData.h"
#include "temperature.h"
#include "timestamp.h"
#include "Flash.h"
#include "param.h"
#include "led.h"

#include "time.h"

#define VERSION	128

#define DISCONNECT_TIMEOUT				30										//timeout for automatic disconnect (milliseconds)
#define TIMEOUT_VALUE					16000

#define CENTRAL_LINK_COUNT				0										/**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT			1										/**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define APP_CFG_CONNECTION_INTERVAL		20										/**< Connection interval used by the central (in milli seconds). This application will be sending one notification per connection interval. A repeating timer will be started with timeout value equal to this value and one notification will be sent everytime this timer expires. */

#define APP_TIMER_PRESCALER				0										/**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE			2										/**< Size of timer operation queues. */

#define CONNECTABLE_ADV_TIMEOUT			BLE_GAP_ADV_TIMEOUT_GENERAL_UNLIMITED	/**< Time for which the device must be advertising in connectable mode (in seconds). */

#define SLAVE_LATENCY					0										/**< Slave latency. */
#define CONN_SUP_TIMEOUT				MSEC_TO_UNITS(4000, UNIT_10_MS)			/**< Connection supervisory timeout (4 seconds). */

#define COMPANY_IDENTIFIER				0x0112									/**< Company identifier */

#define LOCAL_SERVICE_UUID				0x1820									/**< Proprietary UUID for local service. */

#define DEAD_BEEF						0xDEADBEEF								/**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define FLASH_SAVE_ADDRESS	0x0001FC00

ble_uuid_t g_SUUID = 
{
	.uuid = 0x1820,
	.type = BLE_UUID_TYPE_BLE,
};

/**@brief 128-bit UUID base List. */
static const ble_uuid128_t m_base_uuid128 = {0x15, 0xdb, 0x4f, 0xee, 0xb1, 0x35, 0x11, 0xe3, 0x8f, 0x07, 0x94, 0xde, 0x80, 0x0a, 0x00, 0xF0};
static ble_gap_adv_params_t		m_adv_params;									/**< Parameters to be passed to the stack when starting advertising. */
static uint16_t					m_conn_handle = BLE_CONN_HANDLE_INVALID;		/**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection).*/
static uint16_t					m_service_handle;								/**< Handle of local service (as provided by the BLE stack).*/
static uint32_t g_DisconnectCnt=0;

APP_TIMER_DEF(g_AppTimer);
APP_TIMER_DEF(g_LedOffTimer);

static uint32_t g_SecCnt = 0;
extern int8_t g_CurrentTemp;
extern ble_gatts_char_handles_t  g_BeaconTimestampHandle;  
extern ble_gatts_char_handles_t  g_TempOffsetHandle;  
extern ble_gatts_char_handles_t  g_TempLoggingPeriodHandle; 
extern ble_gatts_char_handles_t  g_TempFifoSizeHandle;  
extern ble_gatts_char_handles_t  g_StartLogTimeHandle;             
extern ble_gatts_char_handles_t  g_MinTempHandle;  
extern ble_gatts_char_handles_t  g_MaxTempHandle;  
extern ble_gatts_char_handles_t  g_PowerHandle;
extern ble_gatts_char_handles_t  g_PeriodHandle;
extern ble_gatts_char_handles_t  g_RatioHandle;
extern ble_gatts_char_handles_t  g_BlinkHandle;
extern int8_t g_ReadTempBuf[TEMP_HOUR_QNT][13];
extern uint32_t g_TempFifoInd;
extern uint32_t g_TempPosInd;
extern uint8_t g_TempFifoHourInd;
extern uint32_t g_StartLoggingTime;
extern uint8_t g_TempTresholdsChecked;
extern bool g_FifoFull;

bool SaveParameters(void);
bool LoadParameters(void);

void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    
    ble_gap_addr_t addr;

    err_code = sd_ble_gap_address_get(&addr);    
    APP_ERROR_CHECK(err_code);
	
	
	/* from ble_advdata.c
	// Types of LE Bluetooth Device Address AD type
	#define AD_TYPE_BLE_DEVICE_ADDR_TYPE_PUBLIC 0UL
	#define AD_TYPE_BLE_DEVICE_ADDR_TYPE_RANDOM 1UL
	*/
	//set to be sure that it is public
	addr.addr_type = 0;//AD_TYPE_BLE_DEVICE_ADDR_TYPE_PUBLIC;
	sd_ble_gap_address_set(BLE_GAP_ADDR_CYCLE_MODE_NONE, &addr); // BLE_GAP_ADDR_CYCLE_MODE_NONE-???
	
	char name[7];
    Mac2Str((uint8_t *)name, addr.addr, 3);
	name[6] = 0;
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)name, 
                                          strlen(name));
    APP_ERROR_CHECK(err_code);
    
    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    // Set GAP Peripheral Preferred Connection Parameters (converting connection interval from
    // milliseconds to required unit of 1.25ms).
    gap_conn_params.min_conn_interval = (4 * APP_CFG_CONNECTION_INTERVAL) / 5;
    gap_conn_params.max_conn_interval = (4 * APP_CFG_CONNECTION_INTERVAL) / 5;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

static void connectable_adv_init(uint16_t _interval)
{
    // Initialize advertising parameters (used when starting advertising).
    memset(&m_adv_params, 0, sizeof(m_adv_params));
    
    m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND ;
    m_adv_params.p_peer_addr = NULL;                               // Undirected advertisement
    m_adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    m_adv_params.interval    = _interval;
    m_adv_params.timeout     = CONNECTABLE_ADV_TIMEOUT;
}

static void advertising_data_init(void)
{
    uint32_t                   err_code;
    ble_advdata_t              advdata;
    ble_advdata_manuf_data_t   manuf_data;
    uint8_t                    flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
	
    // Build and set advertising data
    memset(&advdata, 0, sizeof(advdata));

    manuf_data.company_identifier = COMPANY_IDENTIFIER;
    manuf_data.data.size          = sizeof(g_AdvData);
    manuf_data.data.p_data        = (uint8_t *)&g_AdvData;
	
    advdata.flags                 = flags;
    advdata.p_manuf_specific_data = &manuf_data;
	advdata.name_type = BLE_ADVDATA_FULL_NAME;
	advdata.uuids_complete.uuid_cnt = 1;
	advdata.uuids_complete.p_uuids  = &g_SUUID;
	
    err_code = ble_advdata_set(&advdata, NULL);
    APP_ERROR_CHECK(err_code);
}

static void service_add(void)
{
	ble_uuid_t  service_uuid;
	uint32_t    err_code;

	service_uuid.uuid = LOCAL_SERVICE_UUID;

	err_code = sd_ble_uuid_vs_add(&m_base_uuid128, &service_uuid.type);
	APP_ERROR_CHECK(err_code);
	
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &m_service_handle);
	APP_ERROR_CHECK(err_code);

	// Add characteristics
	RegisterPowerChar(service_uuid.type);
	RegisterPeriodChar(service_uuid.type);
	//RegisterRatioChar(service_uuid.type);
	RegisterBlinkChar(service_uuid.type);
	RegisterBeaconTimestampChar(service_uuid.type);
	RegisterTempOffsetChar(service_uuid.type);
	RegisterTempLoggingPeriodChar(service_uuid.type);
	RegisterMinTempChar(service_uuid.type);
	RegisterMaxTempChar(service_uuid.type);
	RegisterTempFifoBuffersChar(service_uuid.type);
	RegisterStartLogTimeChar(service_uuid.type);
}

static void advertising_start(void)
{
	sd_ble_gap_adv_start(&m_adv_params);
}

static void on_write(ble_evt_t * p_ble_evt)
{
	ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
	if(p_evt_write->len==4 || p_evt_write->len==8)
	{
		if( //(p_evt_write->handle == g_BeaconTimestampHandle.value_handle) || //Timestamp handle 
				(p_evt_write->handle == g_TempOffsetHandle.value_handle) || //Temperature offset handle
				(p_evt_write->handle == g_TempLoggingPeriodHandle.value_handle) || //Temperature logging period 
				(p_evt_write->handle == g_MinTempHandle.value_handle) || //Minimum temperature handle
				(p_evt_write->handle == g_MaxTempHandle.value_handle) || //Maximum temperature handle
				(p_evt_write->handle == g_PowerHandle.value_handle) || //Power handle 
				(p_evt_write->handle == g_PeriodHandle.value_handle) || //Adv period handle
				//(p_evt_write->handle == g_RatioHandle.value_handle) || //Adv ratio
				(p_evt_write->handle == g_BlinkHandle.value_handle) //blink
			)
		{
			SaveParameters();
		}
		
		if(p_evt_write->handle == g_PowerHandle.value_handle)
			sd_ble_gap_tx_power_set((int8_t)g_Params.power);
		
		if(p_evt_write->handle == g_PeriodHandle.value_handle)
		{
			connectable_adv_init(MSEC_TO_UNITS(g_Params.advPeriod, UNIT_0_625_MS));
			advertising_start();
		}

		if(p_evt_write->handle == g_StartLogTimeHandle.value_handle)
		{
			RestartTempLogging(g_StartLoggingTime);
		}
	}
}

static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t err_code;    
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
						g_DisconnectCnt = DISCONNECT_TIMEOUT;
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            advertising_start();
				
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle,
                                                 NULL,
                                                 0,
                                                 BLE_GATTS_SYS_ATTR_FLAG_SYS_SRVCS | BLE_GATTS_SYS_ATTR_FLAG_USR_SRVCS);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_TIMEOUT:
            break;
            
        case BLE_GATTS_EVT_WRITE:
            on_write(p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}

static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    on_ble_evt(p_ble_evt);
}

static void sys_evt_dispatch(uint32_t sys_evt)
{
	FlashHandleSysEvent(sys_evt);
}

static void ble_stack_init(void)
{
    uint32_t err_code;
    
    nrf_clock_lf_cfg_t clock_lf_cfg;
	clock_lf_cfg.source = NRF_CLOCK_LF_SRC_RC;
	clock_lf_cfg.rc_ctiv = 2;
	clock_lf_cfg.rc_temp_ctiv = 1;
	clock_lf_cfg.xtal_accuracy = 0;
    
    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
    
    ble_enable_params_t ble_enable_params;
    err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                    PERIPHERAL_LINK_COUNT,
                                                    &ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);
    
    // Enable BLE stack.
    err_code = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);
    
    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}

static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

static void Application()
{
	if (!(g_SecCnt%g_Params.tempLoggingPeriod))
	{
		LogTemp();
	}
	g_AdvData.temp = GetScaledTemp();
	g_AdvData.temp |= (g_TempTresholdsChecked & 0xC0); // add tresholds data
	g_Params.currentTimestamp++;
	
	if(g_Params.blink)
	{
		if(++g_BlinkPacketCnt>=g_Params.blink)
		{
			g_BlinkPacketCnt = 0;
			LEDS_LIGHT_ON(LED_BLUE_MASK);
			app_timer_start(g_LedOffTimer, APP_TIMER_TICKS(1000, BLINK_TIME_MS), NULL);
		}
	}
	else
	{
		LEDS_LIGHT_OFF(LED_BLUE_MASK);
	}	
}

static void UpdateAdvPacket()
{
	uint32_t numOfPackets = g_FifoFull ? TEMP_HOUR_QNT : (g_TempFifoInd + 1);
	memset(g_AdvData.temp_hour_buffer, 0, ADV_TEMP_BUFF_LENGTH);
	
	if(!g_AdvData.packet_number)
	{
		g_AdvData.hour_index = 0;
		g_AdvData.temp_hour_buffer[0] = numOfPackets; //<total number of packets to expect - 1 byte>
		memcpy(g_AdvData.temp_hour_buffer+1, &g_StartLoggingTime, sizeof(g_StartLoggingTime));
	}
	else
	{
		uint32_t index = g_AdvData.packet_number - 1;
		g_AdvData.hour_index = g_ReadTempBuf[index][0];
		g_AdvData.temp_hour_buffer[0] = g_ReadTempBuf[index][1]; //<first 5 min period temperature value - 1 byte>
		int8_t delta;
		uint8_t	validQnt = 0;
		for (int i=0; i<(TEMP_VALUES_PER_HOUR_QNT-1); i++)
		{
			if((uint8_t)(g_ReadTempBuf[index][i+2]) != 0x80)
			{
				++validQnt;
				delta = g_ReadTempBuf[index][i+2] - g_ReadTempBuf[index][i+1];
				delta = (delta < -8) ? -8 : ( (delta > 7) ? 7 : delta );
				if(!(i%2))
					g_AdvData.temp_hour_buffer[1 + i/2] |= (delta << 4);
				else
					g_AdvData.temp_hour_buffer[1 + i/2] |= (delta & 0x0F);
			}
			else
				break;
		}
		g_AdvData.temp_hour_buffer[ADV_TEMP_BUFF_LENGTH-1] |= ((validQnt + 1) & 0x0F);
	}
	
	advertising_data_init();
	if(++g_AdvData.packet_number >= numOfPackets + 1)
		g_AdvData.packet_number = 0;
}

static void timeout_handler(void * p_context)
{
	Application();

	if(g_SecCnt % (g_Params.advPeriod / 1000) == 0)
	{
		UpdateAdvPacket();
	}
		
	if(m_conn_handle != BLE_CONN_HANDLE_INVALID && g_DisconnectCnt && !(--g_DisconnectCnt))
	{
		sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);		
	}
	g_SecCnt++;
}

static void led_off_timeout_handler(void * p_context)
{
	LEDS_LIGHT_OFF(LED_BLUE_MASK);
}

static void timers_init(void)
{
    uint32_t err_code;

    // Initialize timer module
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

    // Create timers
    err_code = app_timer_create(&g_AppTimer, APP_TIMER_MODE_REPEATED, timeout_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_start(g_AppTimer, APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER), NULL);
	APP_ERROR_CHECK(err_code);
	
    err_code = app_timer_create(&g_LedOffTimer, APP_TIMER_MODE_SINGLE_SHOT, led_off_timeout_handler);
    APP_ERROR_CHECK(err_code);	
}

bool SaveParameters(void)
{
	FlashErasePage(FLASH_SAVE_ADDRESS);
	return true;
}

bool LoadParameters(void)
{
	g_TempFifoHourInd = 1;
	g_ReadTempBuf[0][0] = g_TempFifoHourInd;
	memset(&(g_ReadTempBuf[0][1]),0x80,sizeof(g_ReadTempBuf)-1);
	uint32_t check = 0;
	FlashRead(FLASH_SAVE_ADDRESS, (uint8_t*)(&check), sizeof(check));
	if (check == VERSION/*0x12345678*/) //parameters were saved
	{
		FlashRead(FLASH_SAVE_ADDRESS, (uint8_t*)(&g_Params), sizeof(g_Params));
		g_Params.currentTimestamp = 0;
	}
	else
	{
		g_Params.checkField = VERSION;
		SaveParameters();
	}
	return true;
}

void WriteParametersToFlash(bool a)
{
	FlashWrite(FLASH_SAVE_ADDRESS, (uint8_t*)(&g_Params), sizeof(g_Params));
}

int main(void)
{	
	uint32_t err_code;
	FlashSetEraseCallback(WriteParametersToFlash);
	LEDS_CONFIGURE(LEDS_M);
	
	timers_init();
	
	ble_stack_init();  	
	gap_params_init();
	err_code = sd_ble_gap_tx_power_set((int8_t)g_Params.power);    
	APP_ERROR_CHECK(err_code);
	
	LoadParameters();
	
	connectable_adv_init(MSEC_TO_UNITS(g_Params.advPeriod, UNIT_0_625_MS));
	service_add();

	
	advertising_data_init();
	advertising_start();
		
	for (;;)
	{
		power_manage();		
	}
}
