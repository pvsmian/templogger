#include "AdvData.h"
#include "ble_gap.h"

#define SWAP_BYTES(x) (((x) << 8 & 0xff00) | ((x) >> 8 & 0xff))
#define VERSION_NUMBER	0x61

struct AdvData g_AdvData = 
{
	.version = VERSION_NUMBER,
	.temp = 0,
	.packet_number = 0,
	.hour_index = 0,
	.temp_hour_buffer = {0xff}
};

uint8_t hex_to_char(uint8_t hex)
{
    return hex > 9 ? hex + 0x37 : hex + 0x30;
}

void Mac2Str(uint8_t * _str, const uint8_t * _mac, int _len)
{
    for(int i = 0; i < _len * 2; i += 2)
    {
        _str[i] = hex_to_char(_mac[_len - 1 - i / 2] >> 4);
        _str[i + 1] = hex_to_char(_mac[_len - 1 - i / 2] & 0xF);
    }
}
