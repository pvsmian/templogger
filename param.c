#include "param.h"
#include "gatt.h"
#include "temperature.h"

#include "ble.h"
#include "ble_gap.h"
#include "string.h"
#include "app_error.h"

ble_gatts_char_handles_t  g_PowerHandle;
ble_gatts_char_handles_t  g_PeriodHandle;
ble_gatts_char_handles_t  g_RatioHandle;
ble_gatts_char_handles_t  g_BlinkHandle;


struct Param g_Params = 
{
	.checkField = 0x12345678,
	.currentTimestamp = 0,
	.tempOffset = 0,
	.tempLoggingPeriod = DEFAULT_TEMP_LOGGING_INTERVAL,
	.power = DEFAULT_POWER_LEVEL,
	.advPeriod = DEFAULT_ADV_PERIOD,
	.blink = DEFAULT_BLINK,
	.minTemp = DEFAULT_MIN_TEMP,
	.maxTemp = DEFAULT_MAX_TEMP,
};

void RegisterPowerChar(const uint8_t _uuid_type)
{
	  uint8_t power_ud[] = "Power level";
		RegisterCharacteristic(_uuid_type, power_ud, POWER_CHAR_UUID, (void*)(&(g_Params.power)), sizeof(g_Params.power), &g_PowerHandle, READ|WRITE);
}

void RegisterPeriodChar(const uint8_t _uuid_type)
{
	  uint8_t period_ud[] = "Beacon period";
		RegisterCharacteristic(_uuid_type, period_ud, PERIOD_CHAR_UUID, (void*)(&(g_Params.advPeriod)), sizeof(g_Params.advPeriod), &g_PeriodHandle, READ|WRITE);
}

void RegisterBlinkChar(const uint8_t _uuid_type)
{
	  uint8_t blink_ud[] = "Blink";
		RegisterCharacteristic(_uuid_type, blink_ud, BLINK_CHAR_UUID, (void*)(&(g_Params.blink)), sizeof(g_Params.blink), &g_BlinkHandle, READ|WRITE);
}
