#include "nrf_soc.h"
#include "ble.h"

#define READ	1
#define WRITE	2


void RegisterCharacteristic(const uint8_t _uuid_type, const uint8_t* _ud, uint16_t _uuid, void* _p_value, uint16_t _size_value, ble_gatts_char_handles_t*  _p_handle, uint8_t _rw_mask);
