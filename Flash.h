#ifndef __FLASH_H
#define __FLASH_H

#include <stdint.h>
#include <stdbool.h>

typedef void (FlashCallback) (bool);
	
// A page size is 1024 bytes
uint32_t FlashErasePage(uint32_t _page_address);

// _data must point to global or static variable and it size must be %4 = 0. 
// Also _data must be aligned to 4
uint32_t FlashWrite(uint32_t _address, const uint8_t * _data, uint32_t _len);

// Call it in sys_evt_dispatch()
void FlashHandleSysEvent(uint8_t _event);

void FlashRead(uint32_t _address, uint8_t * _data, uint32_t _len);
	
void FlashSetEraseCallback(FlashCallback * _callback);
void FlashSetWriteCallback(FlashCallback * _callback);
	
#endif
