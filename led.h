#include "stdint.h"
#include "nrf_gpio.h"

#define BLINK_TIME_MS	10

#define LED_BLUE      15
#define LED_RED       16

#define LED_BLUE_MASK    (1<<LED_BLUE)
#define LED_RED_MASK    (1<<LED_RED)

#define LEDS_M      (LED_BLUE_MASK | LED_RED_MASK )
#define LEDS_L { LED_BLUE, LED_RED}
#define LEDS_N    2


#define LEDS_LIGHT_ON(leds_mask) do {  NRF_GPIO->OUTSET = (leds_mask); } while (0)

#define LEDS_LIGHT_OFF(leds_mask) do {  NRF_GPIO->OUTCLR = (leds_mask); } while (0)

#define LEDS_CONFIGURE(leds_mask) do { uint32_t pin;                  \
                                  for (pin = 0; pin < 32; pin++) \
                                      if ( (leds_mask) & (1 << pin) )   \
                                          nrf_gpio_cfg_output(pin); } while (0)

uint32_t g_BlinkPacketCnt =0;

