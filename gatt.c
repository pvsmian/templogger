#include "gatt.h"

#include "ble.h"
#include "ble_gap.h"
#include "string.h"
#include "app_error.h"

extern uint8_t m_base_uuid_type;
extern uint16_t m_service_handle; 


void RegisterCharacteristic(const uint8_t _uuid_type, const uint8_t* _ud, uint16_t _uuid, void* _p_value, uint16_t _size_value, ble_gatts_char_handles_t*  _p_handle, uint8_t _rw_mask)
{
    uint32_t            err_code;
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          char_uuid;
    ble_gatts_attr_md_t attr_md;
	  
    memset(&cccd_md, 0, sizeof(cccd_md));

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    memset(&char_md, 0, sizeof(char_md));

	char_md.char_props.read   = _rw_mask&READ ? 1 : 0;
    char_md.char_props.write   = _rw_mask&WRITE ? 1 : 0;
    //char_md.char_props.notify       = 1;
    //char_md.char_props.indicate     = 1;
    char_md.char_ext_props.wr_aux   = 1;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;
	char_md.p_char_user_desc        = (uint8_t*)_ud;
    char_md.char_user_desc_size     = (uint8_t)strlen((char *)_ud);
    char_md.char_user_desc_max_size = (uint8_t)strlen((char *)_ud);

    char_uuid.type = _uuid_type;
    char_uuid.uuid = _uuid;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc    = BLE_GATTS_VLOC_USER;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 0;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &char_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = _size_value;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = _size_value;
    attr_char_value.p_value   = (uint8_t*)(_p_value);

    err_code = sd_ble_gatts_characteristic_add(BLE_GATT_HANDLE_INVALID,
                                               &char_md,
                                               &attr_char_value,
                                               _p_handle);
    APP_ERROR_CHECK(err_code);
}
