#include "stdint.h"

#define ADV_TEMP_BUFF_LENGTH	7 // max sizeof(AdvData)=12

extern __packed struct AdvData
{
    uint8_t		version;
    int8_t		temp; 
	uint8_t		packet_number;
	uint8_t		hour_index;
	uint8_t		temp_hour_buffer[ADV_TEMP_BUFF_LENGTH];
	
} g_AdvData;


void Mac2Str(uint8_t * _str, const uint8_t * _mac, int _len);

