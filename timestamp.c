#include "timestamp.h"
#include "gatt.h"
#include "param.h"                      

#include "ble.h"
#include "ble_gap.h"
#include "string.h"
#include "app_error.h"


ble_gatts_char_handles_t  g_BeaconTimestampHandle;                                      /**< GATT characteristic definition handles. */
extern uint8_t m_base_uuid_type;
extern uint16_t m_service_handle; 

void RegisterBeaconTimestampChar(const uint8_t _uuid_type)
{
    uint8_t ud[] = "Beacon Time";
		RegisterCharacteristic(_uuid_type, ud, BEACONTIMESTAMP_CHAR_UUID, (void*)(&g_Params.currentTimestamp), sizeof(g_Params.currentTimestamp), &g_BeaconTimestampHandle, READ);
}

