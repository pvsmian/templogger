#include "temperature.h"
#include "gatt.h"
#include "param.h"
#include "time.h"

#include "ble.h"
#include "ble_gap.h"
#include "string.h"
#include "app_error.h"


int8_t g_ReadTempBuf[TEMP_HOUR_QNT][TEMP_VALUES_PER_HOUR_QNT+1] = {0};

uint32_t g_TempFifoInd=0;
uint32_t g_TempPosInd=1;
uint8_t g_TempFifoHourInd=1;
bool g_FifoFull = false;

int8_t g_CurrentTemp=0;
uint8_t g_TempTresholdsChecked = 0;
int8_t tmp=0;
uint32_t g_StartLoggingTime = 0;

//uint32_t g_TempLoggingPeriod = DEFAULT_TEMP_LOGGING_INTERVAL;
//uint32_t g_TempOffset=0;
ble_gatts_char_handles_t  g_TempOffsetHandle;                                      /**< GATT characteristic definition handles. */
ble_gatts_char_handles_t  g_ReadTempBufHandle;                                      /**< GATT characteristic definition handles. */
ble_gatts_char_handles_t  g_TempLoggingPeriodHandle;                                      /**< GATT characteristic definition handles. */
ble_gatts_char_handles_t  g_TempFifoSizeHandle;                                      /**< GATT characteristic definition handles. */
ble_gatts_char_handles_t  g_StartLogTimeHandle;                                      /**< GATT characteristic definition handles. */
ble_gatts_char_handles_t  g_MinTempHandle;                                      /**< GATT characteristic definition handles. */
ble_gatts_char_handles_t  g_MaxTempHandle;                                      /**< GATT characteristic definition handles. */
extern uint8_t m_base_uuid_type;
extern uint16_t m_service_handle; 

extern bool SaveParameters(void);

/**
******************************************************************************
* @brief Scales temperature so it fits a 6-bit field.
* @n - 0		= no value
* @n - 1 		= below -20 C
* @n - 2 - 62 	= - 20 C to + 40 C
* @n - 63		= above 40 C 		
*
* @param  actual temperature value
* @return scaled temperature value
*******************************************************************************/

int8_t ScaleTemperature(int8_t _temp)
{
	if(!(_temp & 0x80)) // not a 2's comp negative number
	{
		if(_temp > 40) return 63;
	}
	else if(_temp < -20) return 1;
	
	return ((_temp + 0x16) & 0x003F); // 6 bits only
	
}

int8_t GetTemp(void)
{	
	int32_t temp;
	
	sd_temp_get(&temp);
	temp /= 4;
	temp += g_Params.tempOffset;
	g_CurrentTemp = temp;
	
	//check tresholds
	if(g_CurrentTemp > g_Params.maxTemp)
		g_TempTresholdsChecked |= 0x80;
	else if(g_CurrentTemp < g_Params.minTemp)
		g_TempTresholdsChecked |= 0x40;
	
	return (g_CurrentTemp);
}

int8_t GetScaledTemp(void)
{
	int8_t ct = GetTemp();
	ct = ScaleTemperature(ct);
	return (ct);
}

void WriteTempFifo(int8_t _value)
{
	g_ReadTempBuf[g_TempFifoInd][g_TempPosInd] = _value;
	uint32_t posQnt=TEMP_VALUES_PER_HOUR_QNT; //quantity of values per hour
	
	if (++g_TempPosInd > (posQnt)) // move to the next buffer
	{
		g_TempPosInd = 1;
		g_TempFifoInd = (g_TempFifoInd + 1) % TEMP_HOUR_QNT;
		memset(&(g_ReadTempBuf[g_TempFifoInd][1]),0x80,posQnt);
		if(!g_TempFifoInd)//full fifo
			g_FifoFull = true;
		
		//hour number
		if(g_Params.currentTimestamp > 1000000000) // timestamp was received
		{
			time_t ts = g_Params.currentTimestamp;
			struct tm lt =  *(localtime(&ts));
		}
		//g_ReadTempBuf[g_TempFifoInd][0] = g_TempFifoInd + hour;
		if (!(++g_TempFifoHourInd))
			g_TempFifoHourInd = 1;
		g_ReadTempBuf[g_TempFifoInd][0] = g_TempFifoHourInd;
		
		//SaveParameters(); //uncomment if save fifo to flash
	}
	
  //TODO: save FIFO and indexes to NVM
}
void LogTemp()
{
	int8_t temp;
	
	temp = GetTemp();
	WriteTempFifo(temp);
}

void RegisterTempOffsetChar(const uint8_t _uuid_type)
{
	uint8_t ud[] = "Temperature offset";
	RegisterCharacteristic(_uuid_type, ud, TEMPOFFSET_CHAR_UUID, (void*)(&(g_Params.tempOffset)), sizeof(g_Params.tempOffset), &g_TempOffsetHandle, READ|WRITE);
}

void RegisterTempLoggingPeriodChar(const uint8_t _uuid_type)
{
	uint8_t ud[] = "Temperature logging period";
	RegisterCharacteristic(_uuid_type, ud, TEMPLOGPER_CHAR_UUID, (void*)(&(g_Params.tempLoggingPeriod)), sizeof(g_Params.tempLoggingPeriod), &g_TempLoggingPeriodHandle, READ|WRITE);
}

void RegisterTempFifoBuffersChar(const uint8_t _uuid_type)
{
	uint8_t ud[]="Temperature FIFO buffer";
	RegisterCharacteristic(_uuid_type, ud, READTEMPBUF_CHAR_UUID, (void*)(&(g_ReadTempBuf[0][0])), sizeof(g_ReadTempBuf), &g_ReadTempBufHandle, READ);
}

void RegisterStartLogTimeChar(const uint8_t _uuid_type)
{
	uint8_t ud[]="Start logging timestamp";
	RegisterCharacteristic(_uuid_type, ud, STARTLOGTIME_CHAR_UUID, (void*)(&g_StartLoggingTime), sizeof(g_StartLoggingTime), &g_StartLogTimeHandle, READ|WRITE);
}

void RegisterMinTempChar(const uint8_t _uuid_type)
{
	uint8_t ud[] = "Minimum temperature";
	RegisterCharacteristic(_uuid_type, ud, MINTEMP_CHAR_UUID, (void*)(&(g_Params.minTemp)), sizeof(g_Params.minTemp), &g_MinTempHandle, READ|WRITE);
}

void RegisterMaxTempChar(const uint8_t _uuid_type)
{
	uint8_t ud[] = "Maximum temperature";
	RegisterCharacteristic(_uuid_type, ud, MAXTEMP_CHAR_UUID, (void*)(&(g_Params.maxTemp)), sizeof(g_Params.maxTemp), &g_MaxTempHandle, READ|WRITE);
}

void RestartTempLogging(uint32_t _start_logging_time)
{
	g_Params.currentTimestamp = g_StartLoggingTime;
	g_TempFifoInd=0;
	g_TempPosInd=1;
	memset(&(g_ReadTempBuf[0][1]),0x80,sizeof(g_ReadTempBuf)-1);
	time_t ts = g_StartLoggingTime;
	struct tm lt =  *(localtime(&ts));
	//g_ReadTempBuf[0][0] = lt.tm_hour;
	g_TempFifoHourInd=1;
	g_ReadTempBuf[0][0] = g_TempFifoHourInd;
	g_TempTresholdsChecked = 0;
	g_FifoFull = false;
}
