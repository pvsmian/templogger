#include "nrf_soc.h"

#define DEFAULT_TEMP_LOGGING_INTERVAL		300	//sec
#define DEFAULT_MIN_TEMP					1
#define DEFAULT_MAX_TEMP					5
//#define TEMP_FIFO_CAPACITY				48
#define CHARACTERISTIC_BUFFER_CAPACITY		20
#define TEMP_HOUR_QNT						39
#define TEMP_VALUES_PER_HOUR_QNT			12


#define MINTEMP_CHAR_UUID		0x9009
#define MAXTEMP_CHAR_UUID		0x900A
#define TEMPOFFSET_CHAR_UUID	0x900B
#define READTEMPBUF_CHAR_UUID	0x900C
#define TEMPLOGPER_CHAR_UUID	0x900D
#define STARTLOGTIME_CHAR_UUID	0x900E
																					 
void RegisterTempOffsetChar(const uint8_t _uuid_type);
void RegisterReadTempCommandChar(const uint8_t _uuid_type);
void RegisterReadTempBufChar(const uint8_t _uuid_type);
void RegisterTempLoggingPeriodChar(const uint8_t _uuid_type);
void RegisterTempFifoSizeChar(const uint8_t _uuid_type);
void RegisterTempFifoBuffersChar(const uint8_t _uuid_type);
void RegisterStartLogTimeChar(const uint8_t _uuid_type);
void RegisterMinTempChar(const uint8_t _uuid_type);
void RegisterMaxTempChar(const uint8_t _uuid_type);
																					                                    
int8_t GetTemp(void);
int8_t GetScaledTemp(void);
void WriteTempFifo(int8_t _value);
uint32_t ReadTempFifo(uint32_t _qnt, int8_t* _buf);
void LogTemp(void);
void RestartTempLogging(uint32_t _start_logging_time);
