#ifndef PARAM_H_INCLUDE
#define PARAM_H_INCLUDE

#include "stdint.h"
#include "app_util.h"

#define POWER_CHAR_UUID     0x9007
#define PERIOD_CHAR_UUID    0x9006
#define RATIO_CHAR_UUID     0x9005
#define BLINK_CHAR_UUID     0x9004

// accepted values for power are -40, -30, -20, -16, -12, -8, -4, 0, and 4 dBm
#define DEFAULT_POWER_LEVEL	4
#define DEFAULT_ADV_PERIOD	2000
#define DEFAULT_BLINK		1000

extern __packed struct Param 
{
	uint32_t	checkField;
	uint32_t	currentTimestamp;
	uint32_t	tempOffset;
	uint32_t	tempLoggingPeriod;
	//uint32_t	tempFifoInd;
	//uint32_t	tempPosInd;
	uint32_t	power;
	uint32_t	advPeriod;
	//uint32_t	advRatio;
	uint32_t	blink;
	int8_t		minTemp;
	int8_t		maxTemp;
	uint8_t		padding[2];
} g_Params __attribute__((aligned (4)));

void RegisterPowerChar(const uint8_t _uuid_type);
void RegisterPeriodChar(const uint8_t _uuid_type);
//void RegisterRatioChar(const uint8_t _uuid_type);
void RegisterBlinkChar(const uint8_t _uuid_type);

#endif //PARAM_H_INCLUDE

